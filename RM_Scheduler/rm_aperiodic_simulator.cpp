#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

//Class to hold periodic tasks
class task {
    public:

    string name;
    int period, comp_time, remainingTime, current_deadline;
    int preemptions, missed_deadlines;

    task()
    {
        name = "";
        period = 0;
        comp_time = 0;
        remainingTime = 0;
        current_deadline = 0;
        preemptions = 0;
        missed_deadlines = 0;
        
    }

    task(string _name, int _computation, int _period) {
    name = _name;
    period = _period;
    comp_time = _computation;
    remainingTime = _computation;
    current_deadline = period;
    preemptions = 0;
    missed_deadlines = 0;

    }

    void setName(string _name)
    {
        name = _name;
    }

    void setPeriod(int _period)
    {
        period = _period;
    }

    void setComp(int _comp)
    {
        comp_time = _comp;
    }

    void setCurrent_Deadline(int deadline)
    {
        current_deadline = deadline;
    }

    int getCurrent_Deadline()
    {
        return current_deadline;
    }

    void setReamaining_Time(int time)
    {
        remainingTime = time;
    }
};
//Class to hold aperiodic tasks
class atask {
    public:

    string name;
    int releasetime, comp_time, remainingTime, deadline;

    atask()
    {
        name = "";
        releasetime = 0;
        comp_time = 0;
        remainingTime = 0;
        deadline = 0;
    }

    atask(string _name, int _computation, int _releasetime, int _deadline)
    {
    name = _name;
    releasetime = _releasetime;
    comp_time = _computation;
    remainingTime = _computation;
    deadline = _deadline;
    }

    void setReamaining_Time(int time)
    {
        remainingTime = time;
    }

};


//Function Declarations
void rm_scheduler(task *, atask *, int, int, int, string);
bool compare_interval(const task &a, const task &b);
bool compare_interval2(const atask &a, const atask &b);


int main(int argc, char* argv[])
{
    string read_file;
    string write_file;

    string entries, runtime;
    string str_atasks;
    int num_atasks;

    //Checks command arguments
    if (argc != 3) {
        cerr << "Invalid arguments. Use ./rm_simulator.exe <input_file> <output_file>";
        return 1;
    }

    //Store arguments
    read_file = argv[1];
    write_file = argv[2];
    
    ifstream myfile (read_file);
    //Read first two lines, grabbing tasks and runtime
    getline(myfile, entries);
    getline(myfile, runtime);

    //convert to integers
    int run_time = stoi(runtime);
    int num_tasks = stoi(entries);


    vector<vector<string>> content, content2;
    vector<string> row, row2;
    string line, word, line2, word2;

    //Read the file line by line, separate by comma, store value into vector array
    if (myfile.is_open() ) {

        //Getting Periodic Tasks
        for (int i = 0; i < num_tasks; i++)
        {
            getline(myfile, line);
            row.clear();

            //Remove whitespace
            line.erase(remove(line.begin(), line.end(), ' '), line.end());

            stringstream str(line);

            while(getline(str, word, ','))
                row.push_back(word);
            content.push_back(row);
        }

        //read how many aperiodic tasks there are
        getline(myfile, str_atasks);
        num_atasks = stoi(str_atasks);

        cout << "There are " << entries << " tasks to be scheduled.\n";
        cout << "There are " << num_atasks << " apperiodic tasks" << endl;
        cout << "The runtime is " << runtime << " ms\n";

        //While there are aperiodic tasks left to read
        for (int i = 0; i < num_atasks; i++)
        {
            getline(myfile, line2);
            row2.clear();

            //Remove whitespace
            line2.erase(remove(line2.begin(), line2.end(), ' '), line2.end());

            stringstream str2(line2);

            while(getline(str2, word2, ','))
                row2.push_back(word2);
            content2.push_back(row2);
        }  
    }
     else {
        cout << "Could not open file\n";
    }

    //Store all tasks in task objects
    task taskList[num_tasks];
     for (int i = 0; i<num_tasks; i++)
    {
            taskList[i] = task(content[i][0], stoi(content[i][1]), stoi(content[i][2]) );
           // cout << taskList[i].name << " " << taskList[i].comp_time << " "<< taskList[i].period << endl;
    }

    atask ataskList[num_atasks];
    for (int i = 0; i<num_atasks; i++)
    {

        ataskList[i] = atask(content2[i][0], stoi(content2[i][1]), stoi(content2[i][2]), run_time);
        //cout << ataskList[i].name << " " << ataskList[i].comp_time << " "<< ataskList[i].releasetime << endl;
    }
    
    //RM Scheduling Function
    rm_scheduler(taskList, ataskList, num_tasks, num_atasks, run_time, write_file);

return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    void rm_scheduler(task *tasks, atask *atasks, int num_tasks, int num_atasks, int run_time, string o_file)
    {

        //Writing to output file
        ofstream outfile;
        outfile.open(o_file);

        outfile << "Runtime: " << run_time << endl;
        
        //Sort all of the tasks by period
        sort(tasks, tasks + num_tasks, compare_interval);

        //test the sorting
        outfile << "List of tasks: " << endl;
        for(int i = 0; i < num_tasks; i++)
        {
            cout << "Tasks: " << tasks[i].name << " Deadline: " << tasks[i].period << endl;
            outfile << "Tasks: " << tasks[i].name << " Deadline: " << tasks[i].period << endl;       
        }

        //Sort aperiodic tasks by release time
        sort(atasks, atasks + num_atasks, compare_interval2);
        outfile << "List of aperiodic tasks: " << endl;
        for(int i = 0; i < num_atasks; i++)
        {
            cout << "Aperiodic Tasks: " << atasks[i].name << " ReleaseTime: " << atasks[i].releasetime << endl;
            outfile << "Aperiodic Tasks: " << atasks[i].name << " ReleaseTime: " << atasks[i].releasetime << endl;
        }

        cout << endl;
        cout << "Beginning rm_scheduling" << endl;
        outfile << endl;
        outfile << "Beginning rm_scheduling" << endl;


        //Queues to hold what tasks are released or not
        int current_task = 0;
        int queue[num_tasks];
        int ap_queue[num_atasks];
        fill_n(ap_queue,num_atasks, -1); //fill array with -1 values

        //Loop through the runtime of the program creating schedule
        for(int cur_time = 0; cur_time < run_time; cur_time++)
        {

            //CHECK TO SEE IF ANY DEADLINES ARE MISSED
            bool deadline_missed = true;
            int counter = 0;
            while(deadline_missed == true)
            {
                if(queue[counter] != -1)
                {
                    if (cur_time >= tasks[counter].current_deadline)
                    {
                        cout << "YOU MISSED A DEADLINE ON TASK:  " << tasks[counter].name << endl;
                        outfile << "YOU MISSED A DEADLINE ON TASK:  " << tasks[counter].name << endl;
                        queue[counter] = -1;
                        current_task = -1;
                        tasks[counter].missed_deadlines++;
                        tasks[counter].setReamaining_Time(tasks[counter].comp_time);
                    }
                }
                if(counter == (num_tasks-1))
                {
                    deadline_missed = false;
                }
                counter++;
            }

          //ADD TASKS TO THE QUEUE TO BE RUN IF TASK HAS BEEN RELEASED
        for (int i = 0; i < num_tasks; i++)
        {
            if (cur_time % tasks[i].period == 0)
            {
              queue[i] = i;
              tasks[i].setCurrent_Deadline(cur_time + tasks[i].period);
            }
        }

          //ADD TASKS TO APERIODIC QUEUE TO BE RUN DURING SLACK TIME
        for (int i = 0; i < num_atasks; i++)
        {
            if (cur_time == atasks[i].releasetime)
            {
            ap_queue[i] = i;
            outfile << cur_time << " APERIODIC TASK " << atasks[i].name << " HAS BEEN RELEASED!" << endl;
            cout << cur_time << " APERIODIC TASK " << atasks[i].name << " HAS BEEN RELEASED!" << endl;
            }
        }

        bool ready_for_execution = false;
        bool no_tasks_found = false;
        int position = 0;

        //CHECK QUEUE TO SEE IF THERE IS AN AVAILABLE TASK TO EXECUTE
        if (current_task == -1)
        {
            while (ready_for_execution == false && no_tasks_found == false)
            {
                if (queue[position] != -1)
                {
                    current_task = position;
                    ready_for_execution = true;

                    outfile << cur_time << ": task " << tasks[current_task].name << " has started executing!" << endl;
                    cout << cur_time << ": task " << tasks[current_task].name << " has started executing!" << endl;
                    
                }
                //IF NO TASKS ARE IN THE QUEUE TO RUN
                else if(position == (num_tasks-1))
                    {
                        no_tasks_found = true;
                    }
                else
                    position++;     
            }    
        }
        //CHECK IF THERE IS A PREEMPTION THAT NEEDS TO RUN
        for(int i = 0; i < current_task; i++)
        {
            if (queue[i] != -1)
            {
                //a preemption has occured!
                outfile << "A PREEMPTION HAS OCCURED on task : " << tasks[current_task].name << endl;
                cout << "A PREEMPTION HAS OCCURED on task : " << tasks[current_task].name << endl;
                tasks[current_task].preemptions++;
                current_task = i;
            }
        }
        
        if(current_task != -1)
        {

        //EXECUTE FOR ONE TIME UNIT IF THERE IS A TASK READY TO COMPUTE
        tasks[current_task].setReamaining_Time(tasks[current_task].remainingTime - 1);
        //cout << cur_time << ": task executing = " << tasks[current_task].name << " time remaining: " << tasks[current_task].remainingTime << endl;

        }
        else
        {
            
        ///////////////////APERIOIDIC TASK HANDLING////////////////////////////
            //cout << cur_time << " : SLACK TIME!" << endl;

            //Check and see if there are any aperiodic tasks that are ready to compute
            int my_counter = 0;
            bool ap_check = false;

            while (ap_check == false)
            {   

                //if there is an ap_task in the ap_queue
                if (ap_queue[my_counter] != -1)
                {
                    
                    //Exectue for one time unit if there is an aperiodic task ready to compute
                    atasks[my_counter].setReamaining_Time(atasks[my_counter].remainingTime - 1);

                    outfile << cur_time << ": aperiodic task executing = " << atasks[my_counter].name << " time remaining: " << atasks[my_counter].remainingTime << endl;
                    cout << cur_time << ": aperiodic task executing = " << atasks[my_counter].name << " time remaining: " << atasks[my_counter].remainingTime << endl;
                
                    //When finished executing, remove from queue
                    if (atasks[my_counter].remainingTime == 0)
                    {
                        ap_queue[my_counter] = -1;
                        outfile << cur_time << ": APPERIODIC TASK " << atasks[my_counter].name << " HAS FINISHED EXECUTING!" << endl;
                        cout << cur_time << ": APPERIODIC TASK " << atasks[my_counter].name << " HAS FINISHED EXECUTING!" << endl;
                    }   
                    ap_check = true;
                }
                //Break if no ap_tasks to run
                else if (my_counter == (num_atasks-1))
                { 
                    ap_check = true;
                }
                else {
                    my_counter++;
                }
            }
        }

        //If task finishes, remove from queue, reset execution time
        if (tasks[current_task].remainingTime == 0)
        {
            outfile << cur_time << ": task " << tasks[current_task].name << " has finished executing!" << endl;
            cout << cur_time << ": task " << tasks[current_task].name << " has finished executing!" << endl;
            tasks[current_task].setReamaining_Time(tasks[current_task].comp_time);
            queue[current_task] = -1;
            current_task = -1;
        }

        } //end runtime loop

        //BEGIN REPORTING 
        outfile << "RM SCHEDULING FINISHED" << endl;
        outfile << "---------REPORT---------" << endl;

        //Report on total tasks missed and premptions
        for(int i = 0; i < num_tasks; i++)
        {
            outfile << "Task: " << tasks[i].name << " Deadline misses: " << tasks[i].missed_deadlines << " Number of times preempted: " << tasks[i].preemptions << endl;    
        }

        //Check and report if any aperiodic tasks did not finish in the runtime
        for (int i = 0; i < num_atasks; i++)
        {
            if (ap_queue[i] != -1)
            {
                outfile << "Aperiodic task: \'" << atasks[i].name << "\' MISSED ITS DEADLINE AND DID NOT FINISH" << endl;
            }
            else if (i == (num_atasks-1))
            {
                outfile << "No aperiodic tasks were missed!" << endl;
            }
        }

        outfile.close();
    } 


//Sorting helper functions
    bool compare_interval(const task &a, const task &b)
    {
        return a.period < b.period;
    }

    bool compare_interval2(const atask &a, const atask &b)
    {
        return a.releasetime < b.releasetime;
    }
