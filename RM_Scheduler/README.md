## RM Scheduling
#### By: Tyler George

### Instructions:
 * RM Scheduler WITH aperiodic tasks:
    * Go to RM_Aperiodic_Simulator
    * Compile the .cpp file if no .exe file exists
    * Run ./rm_aperiodic_simulator.exe {input.txt} {output.txt}
         * input.txt and output.txt are your input and output files.

### Test Cases:
* The test cases are :
    * test1.txt
    * mytest.txt
        * Both have utilization between .9 and 1
* Test outputs are :
    * test1output.txt
    * myoutput.txt

    
