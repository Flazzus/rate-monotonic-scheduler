# Rate Monotonic Scheduler

Rate monotonic scheduling program.  Schedules periodic and aperiodic tasks, documents missed tasks, finished tasks, and preempted tasks. C++

# Description

This program implements a rate monotonic (RM) scheduler.  It can be given both periodic and aperiodic tasks and schedules them if possible.  The program documents all missed tasks, finished tasks, and preempted tasks.  RM scheduling prioritises tasks with the shortest period first.  This program reads from a file and outputs the result to a file. Examples tests and results are given.

